package com.commit451.gitlab.activity

import androidx.appcompat.app.AppCompatActivity
import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider
import com.commit451.gitlab.App
import com.commit451.gitlab.api.GitLab
import com.commit451.gitlab.model.Account

/**
 * Base activity for others to derive from
 */
abstract class BaseActivity : AppCompatActivity() {

    val scopeProvider: AndroidLifecycleScopeProvider by lazy {
        AndroidLifecycleScopeProvider.from(
            this
        )
    }

    val account: Account
        get() = App.get().gitLab.account

    val gitLab: GitLab
        get() = App.get().gitLab
}
